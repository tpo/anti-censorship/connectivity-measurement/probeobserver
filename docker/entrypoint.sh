#!/bin/bash
pushd /bridgetest/
source ./conf/script.sh
if [ -z $SKIP_SNOWFLAKE ]
then

SNOWFLAKE_BRIDGELINE_REGIONNAME=common ./probetest.sh snowflake $SITENAME

if [ $SNOWFLAKE_BRIDGELINE_REGIONNAME != "" ]
then
./probetest.sh snowflake $SITENAME
fi

fi
./probetest.sh obfs4 $SITENAME
popd
